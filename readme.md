This library essentially provides GZipStream for use in WP7 applications.

The WebClient and HttpWebRequest implementations in the Windows Phone SDK don't automatically deflate gzipped responses, so it is necessary to do it manually. Below is a code snippet containing two methods taken from my Twitter app Brink that demonstrates how to do deal with receiving a HttpWebResponse that is gzipped:


	private static void HandleCallback(IAsyncResult result, NetCallback callback) {
		try {
			HttpWebResponse response = (result.AsyncState as HttpWebRequest).EndGetResponse(result) as HttpWebResponse;
			String responseText;

			if (response.Headers[HttpRequestHeader.ContentEncoding] != null &&
			response.Headers[HttpRequestHeader.ContentEncoding].Contains("gzip")) {
				GZipStream gstream = new GZipStream(response.GetResponseStream(), CompressionMode.Decompress);
				responseText = gstreamToString(gstream, (int)gstream.Length);
			}
			else {
				responseText = StreamUtility.streamToString(response.GetResponseStream(), (int)response.ContentLength);
			}
			ThreadUtility.NextTick(() => 
				callback(new NetResult(){ IsSuccess = true, ResponseText = responseText, Response = response })
			);
		}
		catch (WebException wex) {
			HttpWebResponse response = (HttpWebResponse)wex.Response;
			String errorText = "Unknown Error";
			if (response != null) {
				try {
					errorText = (new StreamReader(wex.Response.GetResponseStream()).ReadToEnd());
				}
				catch (Exception e) {
					// Dump
				}
			}
			ThreadUtility.NextTick(()=> 
				callback(new NetResult() { IsSuccess = false, ResponseText = errorText, Response = response })
			);
		}
	}
	
	
	public static String gstreamToString(GZipStream stream, int contentLength) {
		StreamReader reader = new StreamReader(stream);
		String str = reader.ReadToEnd();
		reader.Close();
		return str;
	}